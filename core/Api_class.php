<?php


namespace core;

use config\Api_config;

class Api_class
{

    const ERROR_CODES = [
        301 => 'Moved permanently',
        400 => 'Bad request',
        401 => 'Unauthorized',
        403 => 'Forbidden',
        404 => 'Not found',
        500 => 'Internal server error',
        502 => 'Bad gateway',
        503 => 'Service unavailable'
    ];


    public static function curl_post_request($data, $url)
    {
        sleep(1);
        $link = 'https://' . Api_config::$domain . '.amocrm.ru/' . $url;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '\cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '\cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);

        if ($code != 200 && $code != 204)
            throw new \Exception(in_array($code, static::ERROR_CODES)
                ? static::ERROR_CODES[$code]
                : 'Undescribed error', $code);

        $response = json_decode($out, TRUE);

        return $response;
    }

    public static function curl_get_request($url)
    {
        sleep(1);
        $link = 'https://' . Api_config::$domain . '.amocrm.ru/' . $url;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, FALSE);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($out, TRUE);

        return $response['_embedded'];
    }

    public static function auth()
    {
        return self::curl_post_request(Api_config::$user, 'private/api/auth.php?type=json');
    }

    public static function send(array $entities_array, $url, $action = 'add')
    {
        $result[$action] = $entities_array;
        $response = self::curl_post_request($result, $url);

        foreach ($response['_embedded']['items'] as $v)
            if (is_array($v)) {
                $output[] = $v['id'];
                if ($v['id'] == 0) {
                    throw new \Exception('ID сущности указан неверно', 400);
                }
            }

        return $output;
    }

    public static function find($id, $url)
    {
        $result = static::curl_get_request($url . '?id=' . $id);
        if (is_null($result)) {
            $entity_name = substr($url, 7);
            throw new \Exception($entity_name . ' not found ', 404);
        }

        return $result['items'][0];
    }

    public static function find_all($url)
    {
        $result = static::curl_get_request($url);

        if (isset($result['items'])) {
            return $result['items'];
        }
        $items_name = substr($url, 20);

        return $result[$items_name];
    }
}
