<?php


namespace App\Controllers;

use App\Controller;
use App\Entities\Company;
use App\Entities\Contact;
use App\Entities\Lead;

class Wiget_controller extends Controller
{
    public static function index()
    {
        $res_leads = json_decode($_POST['leads'], false);

        foreach ($res_leads as $res_lead) {
            $lead = Lead::find($res_lead->id);
            $lead_arr['name'] = $lead->name;
            $lead_arr['created_at'] = $lead->created_at;
            //$lead_arr['tags'] = implode(',', $lead->tags);


            $lead_arr['custom_fields'] =
                !empty($lead->custom_fields) ?
                    array_reduce($lead->custom_fields, function ($string, $custom_field)
                    {
                        return $string .= $custom_field['values'][0]['value'];
                    }, '')
                    : 'NULL';
            $lead_arr['company_name'] =


            !empty($lead->company) ?
                Company::find($lead->company['id'])->name
                : 'NULL';

            $lead_arr['contacts'] =
                !empty($lead->contacts) ? implode(',',
                    array_map(function ($contact_id)
                        {
                            return Contact::find($contact_id)->name;
                        }, $lead->contacts['id']))
                    : 'NULL';

            $result[] = $lead_arr;

        }


        $file_name = hash('ripemd160', (string)rand(0, 10));
        $fp = fopen('csv/' . $file_name . '.csv', 'w');
        foreach ($result as $fields) {
            fputcsv($fp, $fields);
            fwrite($fp, PHP_EOL);
        }


       echo $file_name;

    }
}