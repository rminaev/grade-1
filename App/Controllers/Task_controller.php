<?php

namespace App\Controllers;

use App\Entities\Company;
use App\Entities\Contact;
use App\Entities\Lead;
use App\Entities\Customer;
use App\Entities\Custom_field;
use App\Entities\Note;
use App\Entities\Task;
use App\Entities\User;
use core\Api_class;

class Task_controller
{
    public static function task_1($count)
    {
        Api_class::auth();

        try {
            if ($count < 0 || $count > 5000) {
                return 'невалидное кол-во';
            }

            for ($i = 0; $i < (int)$count; $i++) {
                $companies[] = new Company();
            }

            $company_ids = Company::mass($companies);

            $leads = array_map(function ($company_id) {
                $lead = new Lead();
                $lead->company_id = $company_id;
                return $lead;
            }, $company_ids);

            $leads_ids = Lead::mass($leads);

            $contacts = array_map(function ($lead_id) {
                $contact = new Contact();
                $contact->leads_id = $lead_id;
                return $contact;

            }, $leads_ids);

            $contacts_ids = Contact::mass($contacts);

            $customers = array_map(function ($contact_id) {
                $customer = new Customer();
                $customer->contacts_id = [$contact_id];
                return $customer;
            }, $contacts_ids);

            Customer::mass($customers);

            $mylti = new Custom_field();
            $mylti->set_element_type('contact');
            $mylti->set_field_type('MULTISELECT');
            $mylti->enums = range(0, 10);
            $mylti->save();
            $contacts = Contact::find_all(500);
            $enums = $mylti->get_enums_ids('contacts');

            foreach ($contacts as $contact) {
                shuffle($enums);
                $add_enums = array_slice($enums, rand(0, 9));
                $contact->set_custom_field($mylti->id, $add_enums);
                $result[] = $contact;
            }

            Contact::mass($result, 100, 'update');

        } catch (\Exception $e) {

            return $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode();
        }

        return 'OK';
    }

    public static function task_2($params)
    {
        try {

            Api_class::auth();

            $element_type_arr = explode(',', $params['element_type']);
            $custom_fileds = Custom_field::find_all($element_type_arr[1]);
            $fields_type_text = array_filter($custom_fileds, function ($field) {
                return $field->field_type === 1;
            });

            if (count($fields_type_text) > 0) {
                $field_type_text = array_values($fields_type_text)[0];

            } else {
                $field_type_text = new Custom_field();
                $field_type_text->set_field_type(1);
                $field_type_text->set_element_type($element_type_arr[1]);
                $field_type_text->save();
            }

            switch ($element_type_arr[0]) {
                case 1:
                    $entity = Contact::find($params['element_id']);
                    break;
                case 2:
                    $entity = Company::find($params['element_id']);
                    break;
                case 3:
                    $entity = Lead::find($params['element_id']);
                    break;
                case 12:
                    $entity = Customer::find($params['element_id']);
                    break;
            }

            $entity->set_custom_field($field_type_text->id, [['value' => $params['text_value']]]);
            $entity->save();

        } catch (\Exception $e) {
            return $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode();
        }

        return 'OK';
    }


    public static function task_3($params)
    {
        $note = new Note($params);
        if ($note->note_type == 4) {
            $note->set_text();
        } else {
            $note->set_call_params();
        }
        try {
            Api_class::auth();
            $note->save();
        } catch (\Exception $e) {
            return $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode();
        }
        return 'OK';
    }

    public static function task_4($params)
    {
        try {
            $task = new Task($params);
            $task->task_type = 2;
            $users = User::find_all();
            if (!isset($users[$task->responsible_user_id]))
                return 'Пользователь не найден';
            $task->save();

        } catch (\Exception $e) {
            return $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode();
        }
        return 'OK';
    }

    public static function task_5($id)
    {
        try {
            Api_class::auth();
            $task = Task::find($id);
            $task->is_completed = true;
            $task->save();
        } catch (\Exception $e) {
            return $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode();
        }
        return 'OK';
    }

}
