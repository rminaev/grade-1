<?php
/**
 * Created by PhpStorm.
 * User: dobro
 * Date: 08.09.2018
 * Time: 15:56
 */

namespace App;

use core\Api_class;

class Entity
{
    public static $url;

    public function __set($name, $value)
    {
        $this->{$name} = $value;
    }

    public function __get($name)
    {
        return $this->{$name};
    }

    public function __construct()
    {
        $this->name = substr(md5(mt_rand()), 0, 7);
    }

    public function set_custom_field($c_f_id, array $enums)
    {
        $this->custom_fields = [
            [
                'id' => $c_f_id,
                'values' => $enums
            ]
        ];
    }

    public static function mass(array $array, $chunk_size = 100, $action = 'add')
    {
        if (count($array) > $chunk_size) {
            return array_reduce(array_chunk($array, $chunk_size),
                function ($ids, $chunk, $action) {
                    return array_merge($ids, Api_class::send($chunk, static::$url, $action));
                }, []);

        } else {
            return Api_class::send($array, static::$url, $action);
        }
    }

    public function save()
    {
        if (isset($this->id)) {
            $this->updated_at = time();
            return Api_class::send([$this], static::$url, 'update');
        } else {
            return $this->id = Api_class::send([$this], static::$url)[0];
        }
    }

    public static function find($id)
    {
        $class_name = static::class;
        $entity = new $class_name();
        foreach (Api_class::find($id, static::$url) as $item => $value) {
            $entity->$item = $value;
        }
        return $entity;
    }

    public static function find_all($limit = 100)
    {
        return array_map(function ($array) {
            $class_name = static::class;
            $entity = new $class_name;
            foreach ($array as $item => $value) {
                $entity->$item = $value;
            }
            return $entity;

        }, Api_class::find_all(static::$url, $limit));
    }
}
