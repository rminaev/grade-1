<?php

switch ($_SERVER['REQUEST_URI']) {
    case '/' :
        \App\Controllers\Index_controller::index();
        break;
    case '/?form=task_1' :
        echo \App\Controllers\Task_controller::task_1($_POST['count']);
        break;
    case '/?form=task_2' :
        echo \App\Controllers\Task_controller::task_2($_POST);
        break;
    case '/?form=task_3' :
        echo \App\Controllers\Task_controller::task_3($_POST);
        break;
    case '/?form=task_4' :
        echo \App\Controllers\Task_controller::task_4($_POST);
        break;
    case '/?form=task_5' :
        echo \App\Controllers\Task_controller::task_5($_POST['id']);
        break;
    case '/?ajax':
        \App\Controllers\Wiget_controller::index();
        break;
}

