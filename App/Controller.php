<?php
/**
 * Created by PhpStorm.
 * User: rminaev
 * Date: 11.09.2018
 * Time: 16:32
 */

namespace App;


class Controller
{
    public static function render($template_name)
    {
        include(__DIR__ . '/../Views/' . $template_name . '.php');
    }

}