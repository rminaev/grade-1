<?php

namespace App\Entities;

use App\Entity;

class Contact extends Entity
{
    public static $url = 'api/v2/contacts';
}
