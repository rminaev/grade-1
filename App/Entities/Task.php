<?php

namespace App\Entities;

use App\Entity;

class Task extends Entity
{
    public static $url = 'api/v2/tasks';

    public function __construct($params = [])
    {
        if (!empty($params)) {
            foreach ($params as $name => $value) {
                if ($name == 'complete_till_at')
                    $value = strtotime($value);
                $this->$name = $value;
            }
        }
    }
}
