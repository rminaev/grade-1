<?php

namespace App\Entities;

use App\Entity;

class Note extends Entity
{
    public static $url = 'api/v2/notes';

    public function __construct($params = [])
    {
        if (!empty($params)) {
            foreach ($params as $name => $value) {
                $this->$name = $value;
            }
        }
    }

    public function set_call_params()
    {
        $this->params = [
            'UNIQ' => '676sdfs7fsdf',
            'LINK' => 'www.testweb.ru/test_call.mp3',
            'PHONE' => '84950000001',
            'DURATION' => 58,
            'SRC' => 'asterisk'
        ];
    }

    public function set_text()
    {
        $this->text = substr(md5(mt_rand()), 0, 7);
    }
}
