<?php

namespace App\Entities;

use App\Entity;

class Customer extends Entity
{
    public static $url = 'api/v2/customers';
}
