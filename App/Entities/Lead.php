<?php

namespace App\Entities;

use App\Entity;

class Lead extends Entity
{
    public static $url = 'api/v2/leads';
}
