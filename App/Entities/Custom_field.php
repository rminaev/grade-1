<?php

namespace App\Entities;

use App\Entity;
use config\Api_config;
use core\Api_class;

class Custom_field extends Entity
{
    public $origin;
    public $is_editable = TRUE;
    public static $url = 'api/v2/fields';

    public function __construct()
    {
        parent::__construct();
        $this->origin = Api_config::$origin;
    }

    public function set_element_type($name)
    {
        $element_types = [
            'contact' => 1,
            'lead' => 2,
            'company' => 3,
            'customer' => 12
        ];
        $this->element_type = $element_types[$name];
    }

    public function set_field_type($name)
    {
        $field_types = [
            'TEXT' => 1,
            'NUMERIC' => 2,
            'CHECKBOX' => 3,
            'SELECT' => 4,
            'MULTISELECT' => 5
        ];
        $this->field_type = $field_types[$name];
    }

    public static function find_all($type)
    {
        $url = 'api/v2/account?with=custom_fields';

        return array_map(function ($array) {
            $field = new Custom_field();
            foreach ($array as $item => $value) {
                $field->$item = $value;
            }
            return $field;

        }, Api_class::find_all($url)[$type]);
    }

    public function get_enums_ids($entity_type)
    {
        $custom_fields = Api_class::curl_get_request('api/v2/account?with=custom_fields')['custom_fields'];
        return array_keys($custom_fields[$entity_type][$this->id]['enums']);
    }

}
