<?php

namespace App\Entities;

use App\Entity;

class Company extends Entity
{
    public static $url = 'api/v2/companies';
}
