<?php

spl_autoload_register(function($class) {

    require '../' . $class . '.php';
    require '../vendor/autoload.php';

});
