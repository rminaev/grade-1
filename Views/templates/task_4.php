
<h3>Задание 4</h3>

<form action="/?form=task_4" method="post">

    <div class="form-group">
        <label>Тип сущности</label>
        <select name="element_type" class="form-control" required>
            <option value="1">Контакт</option>
            <option value="2">Сделка</option>
            <option value="3">Компания</option>
            <option value="12">Покупатель</option>
        </select>
    </div>

    <div class="form-group" id="Task_4">
        <label >id сущности</label>
        <input type="number"  name="element_id" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Дата до которой необходимо завершить задачу</label>
        <input type="datetime-local" name="complete_till_at" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Текст задачи</label>
        <input type="text" name="text" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Id ответсвенного пользователя</label>
        <input type="number" name="responsible_user_id" class="form-control" required>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
