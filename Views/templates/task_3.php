
<h3>Задание 3</h3>

<form action="/?form=task_3" method="post">

    <div class="form-group">
        <label>Тип сущности</label>
        <select name="element_type" class="form-control" id="entity_type" required>
            <option value="1">Контакт</option>
            <option value="2">Сделка</option>
            <option value="3">Компания</option>
            <option value="12">Покупатель</option>
        </select>
    </div>
    <div class="form-group" id="Task_1">
        <label >id сущности</label>
        <input type="number"  name="element_id" class="form-control" id="entity_id" required>
    </div>
    <div class="form-group">
        <label>Тип задачи</label>
        <select name="note_type" class="form-control" id="note_type" required>
            <option value="4">Обычное примечание</option>
            <option value="10">Входящий звонок</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

