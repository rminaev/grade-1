<h3>Задание 2</h3>

<form action="/?form=task_2" method="post">

    <div class="form-group">
        <label>Тип сущности</label>
        <select name="element_type" class="form-control" id="entity_type" required>
            <option value="1,contacts">Контакт</option>
            <option value="2,leads">Сделка</option>
            <option value="3,companies">Компания</option>
            <option value="12,customers">Покупатель</option>
        </select>
    </div>
    <div class="form-group" id="Task_1">
        <label >id сущности</label>
        <input type="number"  name="element_id" class="form-control" id="entity_id" required>
    </div>
    <div class="form-group">
        <label>Значение кастом филда</label>
        <input type="text" name="text_value" class="form-control">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
