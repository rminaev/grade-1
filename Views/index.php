<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
<br><br>
<div class="container">
    <?php include_once 'templates/task_1.php'?>
<br><hr><br>
    <?php include_once 'templates/task_2.php'?>
<br><hr><br>
    <?php include_once 'templates/task_3.php'?>
<br><hr><br>
    <?php include_once 'templates/task_4.php'?>
<br><hr><br>
    <?php include_once 'templates/task_5.php'?>
<br><br><br>
</div>

</body>
</html>