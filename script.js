define(['jquery'], function($){
  var CustomWidget = function () {
        var self = this, // 
        system = self.system(), 
        langs = self.langs;  
       
        this.callbacks = {
              settings: function(){
              },
              init: function(){    
                    return true
              },
              bind_actions: function(){
                    $('.g_w_close').click(() => {
                      self.widgetsOverlay(false);
                    }); 
                      
                    return true;
              },
              render: function(){  
                 var html_data ='<div class="nw_form">'+
                    '<div class="g_w_send">Send</div>' +
                    '<div class="g_w_close">Close</div>' + 
                    '<a class="g_w_download" href="" download="leads.csv"></a>' +
                    '</div>';
                      self.render_template(
                    {
                      caption:{
                         class_name:'new_widget',
                              },
                           body: html_data,
                          render : '' 
                       }
                     );
                      
                    return true;
              },            
              dpSettings: function(){              
              },
              advancedSettings: function() {
              },
              destroy: function(){              
              },    
              contacts: { selected: function() {                  
                    }
              },
              leads: { selected: function() {  
                var leadsSelected = JSON.stringify(self.list_selected().selected);
                   $('.g_w_send').click( () => {

                          $.ajax({

                          type: 'POST',

                          url: 'http://localhost/?ajax',


                          data: {leads :leadsSelected},

                          success: function(data){

                                  $(".g_w_download").
                                  html("download").
                                  attr("href","http://localhost/csv/" + data + '.csv' );
                                   }
                               })
                        }); 
                    }
              },
              onSave: function(){        
              }
        };
        return this;
    };
  return CustomWidget;
});